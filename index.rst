

.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>

|FluxWeb| `RSS <https://gdevops.frama.io/opsindev/dagger/rss.xml>`_


.. _dagger_tuto:

====================================
|dagger| **Opsindev dagger**
====================================

- https://github.com/dagger/dagger
- https://github.com/dagger/dagger/commits.atom
- https://dagger.io/
- https://discord.gg/dagger-io
- https://www.youtube.com/@dagger-io/videos

.. toctree::
   :maxdepth: 6

   introduction/introduction
   news/news
   versions/versions
