.. index::
   pair: dagger; CI/CD

.. _dagger_introduction:

=====================================
|dagger| **Dagger introduction**
=====================================

- https://docs.dagger.io/648215/quickstart

What is Dagger ?
===================

Dagger is a programmable CI/CD engine that runs your pipelines in containers.

Welcome to Dagger, a programmable CI/CD engine that runs your pipelines
in containers.

This tutorial introduces you to Dagger and the "Dagger way" of building
portable CI pipelines.

It walks you, step by step, through the process of creating and refining
a CI pipeline to test, build and publish an example application with a
Dagger SDK.

