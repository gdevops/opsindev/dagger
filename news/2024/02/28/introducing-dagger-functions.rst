.. index::
   pair: Dagger; Functions

.. _dagger_functions:

=================================================================================
2024-02-28 **Introducing Dagger Functions**  (dagger 0.10.0)
=================================================================================

- https://dagger.io/blog/introducing-dagger-functions
- :ref:`dagger_0_10_0`

Introduction
================

Developers use Dagger to transform their messy CI scripts into clean code.
This makes their CI simpler, faster, more reliable, more portable - and
more fun to write.

Today we’re taking the fun to a new level, with a feature we call Dagger Functions,
available in :ref:`Dagger 0.10 <dagger_0_10_0>`.

If you want to jump in directly, get started! Otherwise, read on for the details.

What are Dagger Functions ?
=================================

**Dagger Functions are a core feature of the Dagger Engine, which we are expanding
and making available to all developers**.

At the heart of Dagger is a next-generation container engine controlled by a
GraphQL API.

**Using this API, a workflow that once required hundreds of lines of bash and
YAML can be distilled to a few dozen lines of Go, Python or Typescript**.

How can so little code accomplish so much ? **By calling Dagger Functions**.

Functions are the fundamental unit of computing in the Dagger Engine.
Each core engine operation, for example “pull a container image”, “copy a file”,
“forward a TCP port”, is exposed to API clients as a function that they can call.

Calling functions over HTTP is a well-known way to standardize API clients
across many programming languages, but **Dagger adds a crucial innovation: instead
of calling one function at a time, clients can use the GraphQL query language
to chain multiple functions together into a dynamic pipeline, with a single query**.

Thanks to this combination of powerful core functions, a GraphQL API to
compose them, and native SDKs to do so in a familiar language, **Dagger has
made CI-as-code a reality**.

Today we are expanding this core capability of the engine in three major ways:

- First, you can now write your own Dagger Functions - effectively making
  the Dagger API infinitely extensible.
- Second, you can package your Dagger Functions into reusable modules - unlocking
  a whole new world of cross-language collaboration for the devops community
- Third, you can now call Dagger Functions directly from the CLI - making
  them immediately usable without having to write code


