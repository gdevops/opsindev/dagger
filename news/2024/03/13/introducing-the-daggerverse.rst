.. index::
   pair: dagger; Introducing the Daggerverse

.. _daggerverse_2024_03_13:

==============================================
2024-03-13 **Introducing the Daggerverse**
==============================================

- https://dagger.io/blog/introducing-the-daggerverse
- https://daggerverse.dev/

#dagger #ci #daggerverse

Announce
===========

Today we’re excited to introduce the Daggerverse: a searchable index of all
public Dagger Functions.

Two weeks ago we introduced Dagger Functions, a powerful new way to extend
the Dagger API with custom code.

The Dagger API already gave you primitives to stitch together powerful
pipelines out of containers, artifacts, network services, and secrets – all
in type-safe Go, #Python or TypeScript.


ci-tools-dagger-the-new-world
================================

#gitlab #ci #dagger #pipeline

- https://www.puzzle.ch/de/blog/articles/2024/03/01/ci-tools-dagger-the-new-world-part-3-3

This is a small example to show how parts of a GitLab CI pipeline can be
migrated into a Dagger pipeline
